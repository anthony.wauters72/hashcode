L'idée ici est de paralléliser le remplissage et la lecture de la map en s'assurant que nos thread sont safes. Pour cela je pensais utiliser tout d'abord un mutex avec des std::thread, mais je peux simplifier le code en utilisant seulement un std::async pour la lecture de la map.

Ensuite je gère le temps du programme pour avoir un temps limite et etre sur que l'on sorte du programme pour ne pas faire planter le systeme et pouvoir sortir en cas d'echec.


Je n'ai pas réussi à paralléliser la construction des chaines vis à vis du thread de base car je genère constamment les mêmes chaines, j'avoue ne pas voir comment m'en sortir avec un thread async.